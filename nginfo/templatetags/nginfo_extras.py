from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
#@stringfilter
def agencyListStr(obj, join_on):
    """ obj should be a
    """
    if obj:
        if join_on[0] == "1":
            join_on = join_on[1:]
            return join_on.join([ag.short_name for ag in obj.all()])
        elif join_on[0] == "2":
            join_on = join_on[1:]
            return join_on.join([("<a href=\""+ag.link+"\">"+ag.short_name+"</a>") for ag in obj.all()])
        else:
            return join_on.join([ag.institute for ag in obj.all()])
    return ""

@register.filter
def PpgnetBallon(obj):
    if not obj.agency:
        return false
    if len(obj.agency.all()) == 2:
        aglist = obj.agency.all().values_list("short_name", flat=True)
        if "GOP" in aglist and "UPATRAS" in aglist:
            return True
    return False
