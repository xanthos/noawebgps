from django.db import models
from os.path import basename
import math
import sys

class Agency(models.Model):
    short_name = models.CharField(max_length=10)
    institute  = models.CharField(max_length=100)
    department = models.CharField(max_length=100)
    link       = models.CharField(max_length=100)
    email      = models.CharField(max_length=50)

    def __str__(self):
        return self.short_name

class Network(models.Model):
    short_name = models.CharField(max_length=10)
    long_name  = models.CharField(max_length=200)
    description= models.CharField(max_length=200)

    def __str__(self):
        return self.short_name

class Position(models.Model):
    refframe = models.CharField(max_length=20)
    xpos     = models.DecimalField(max_digits=15, decimal_places=4)
    ypos     = models.DecimalField(max_digits=15, decimal_places=4)
    zpos     = models.DecimalField(max_digits=15, decimal_places=4)
    
    def __str__(self):
        xstr = '{:+15.3f}'.format(self.xpos)
        ystr = '{:+15.3f}'.format(self.ypos)
        zstr = '{:+15.3f}'.format(self.zpos)
        return '{:}, {:}, {:}'.format(xstr, ystr, zstr)

    def __to_float__(self):
        return [ float(i) for i in [self.xpos, self.ypos, self.zpos] ]

    def toGeodetic(self):
        x, y, z = self.__to_float__()
        a = 6378137e0 ## ellipsoid.semiMajorAxis()
        f = 1e00/298.257222101e0 ## ellipsoid.flattening()
        ## Functions of ellipsoid parameters.
        aeps2 = a*a*1e-32
        e2    = (2e0-f)*f
        e4t   = e2*e2*1.5e0
        ep2   = 1e0-e2
        ep    = math.sqrt(ep2)
        aep   = a*ep
        ## Compute distance from polar axis squared.
        p2 = x*x + y*y
        ## Compute longitude lon
        if p2 != .0e0:
            lon = math.atan2(y, x)
        else:
            lon = .0e0
        ## Ensure that Z-coordinate is unsigned.
        absz = abs(z)
        if p2 > aeps2: ## Continue unless at the poles
            p   = math.sqrt(p2)
            s0  = absz/a
            pn  = p/a
            zp  = ep*s0 
            c0  = ep*pn 
            c02 = c0*c0 
            c03 = c02*c0 
            s02 = s0*s0 
            s03 = s02*s0 
            a02 = c02+s02 
            a0  = math.sqrt(a02) 
            a03 = a02*a0 
            d0  = zp*a03 + e2*s03 
            f0  = pn*a03 - e2*c03 
            b0  = e4t*s02*c02*pn*(a0-ep) 
            s1  = d0*f0 - b0*s0 
            cp  = ep*(f0*f0-b0*c0) 
            lat = math.atan(s1/cp)
            s12 = s1*s1 
            cp2 = cp*cp 
            hgt = (p*cp+absz*s1-a*math.sqrt(ep2*s12+cp2))/math.sqrt(s12+cp2);
        else: ## Special case: pole.
            lat = math.pi / 2e0;
            hgt = absz - aep;
        ## Restore sign of latitude.
        if z < 0e0:
            lat = -lat
        return [lon, lat, hgt]

    def toMapMarker(self):
        lon, lat, _ = self.toGeodetic()
        return [math.degrees(lat), math.degrees(lon)]

    def tabularGeodetic(self):
        lon, lat, hgt = self.toGeodetic()
        return "<td>{:+10.5f}</td><td>{:9.5f}</td><td>{:7.2f}</td>".format(
            math.degrees(lon), math.degrees(lat), hgt)

def default_created_by_user():
    return 1

class GpsStation(models.Model):
    four_char_id = models.CharField(max_length=4)
    ten_char_id  = models.CharField(max_length=10)
    location     = models.CharField(max_length=200)
    xyz_position = models.ForeignKey(Position, on_delete=models.CASCADE)
    domes_number = models.CharField(max_length=10)
    agency       = models.ManyToManyField(Agency)
    network      = models.ManyToManyField(Network)
    install_date = models.DateField()
    antenna      = models.CharField(max_length=20, default="Unknown")
    receiver     = models.CharField(max_length=20, default="Unknown")
    satsys       = models.CharField(max_length=10) # eg 'GR' is gps, glo
    owned_by     = models.ForeignKey(Agency, on_delete=models.CASCADE, related_name="station_owned_by_agency", default=default_created_by_user)
    status       = models.CharField(max_length=50, default="operational")

    def __str__(self):
        return self.four_char_id

    def hasDomesNumber(self):
        return self.domes_number is not ''

    def installDate(self, dformat):
        return self.install_date.strftime(dformat)

class StationPhoto(models.Model):
    url     = models.CharField(max_length=200, default="")
    path    = models.CharField(max_length=200, default="")
    station = models.ForeignKey(GpsStation, on_delete=models.CASCADE)

class TimeSeries(models.Model):
    url     = models.CharField(max_length=200, default="")
    filename= models.CharField(max_length=200, default="")
    station = models.ForeignKey(GpsStation, on_delete=models.CASCADE)
    agency  = models.ForeignKey(Agency, on_delete=models.CASCADE)

class LogFile(models.Model):
    filename = models.CharField(max_length=200)
    station = models.ForeignKey(GpsStation, on_delete=models.CASCADE)

    def basename(self):
        if not self.filename or self.filename == "":
            return ""
        return basename(self.filename)   

    def dumpAsHtml(self):
        # rootpth="/home/xanthos/Software/noawebgps/static/"
        rootpth = "/home/xanthos/noa-helpos/noa_site/noawebgps/static/"
        with open(rootpth+self.filename, 'r') as ins:
           log = ins.read().replace('\n', '<br>')
        return log
