from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from django.template import loader
import json as simplejson
from nginfo.models import GpsStation, Position, StationPhoto, LogFile, Agency

def index(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    template = loader.get_template('nginfo/index.html')
    context = { 
        'station_list': station_list,
    }
    output = '\n'.join(['{:} {:}'.format(sta, sta.domes_number) for sta in station_list])
    return HttpResponse(template.render(context, request))

def netmap(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    template = loader.get_template('nginfo/netmap.html')
    context = { 
        'station_list': station_list,
    }
    return HttpResponse(template.render(context, request))

def noamaps(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    template = loader.get_template('nginfo/noamaps.html')
    context = { 
        'station_list': station_list,
    }
    return HttpResponse(template.render(context, request))

def data(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    agency_list = Agency.objects.order_by('short_name')
    template = loader.get_template('nginfo/data.html')
    context = {
        'station_list': station_list,
        'agency_list': agency_list,
    }
    return HttpResponse(template.render(context, request))

def intro(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    agency_list = Agency.objects.order_by('short_name')
    template = loader.get_template('nginfo/intro.html')
    context = {
        'station_list': station_list,
        'agency_list': agency_list,
    }
    return HttpResponse(template.render(context, request))

def research(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    #agency_list = Agency.objects.order_by('short_name')
    template = loader.get_template('nginfo/research.html')
    context = {
        'station_list': station_list,
        #'agency_list': agency_list,
    }
    return HttpResponse(template.render(context, request))

def station(request, gpsstation_four_char_id):
    station  = get_object_or_404(GpsStation, four_char_id=gpsstation_four_char_id)
    photos   = StationPhoto.objects.filter(station__four_char_id=gpsstation_four_char_id)
    try:
        logfile  = LogFile.objects.get(station__four_char_id=gpsstation_four_char_id)
    except:
        logfile = None
    agency_list = GpsStation.objects.get(four_char_id=gpsstation_four_char_id).agency.all()
    station_list = GpsStation.objects.order_by('four_char_id')
    template = loader.get_template('nginfo/station.html')
    context = { 
        'station_list': station_list,
        'station': station,
        'photos': photos,
        'logfile': logfile,
        'agency_list': agency_list,
        'json_filename': "json/" + station.four_char_id + ".json",
    }
    return HttpResponse(template.render(context, request))

def nettable(request, sort_by="four_char_id"):
    station_list = GpsStation.objects.order_by(sort_by)
    json_list    = []
    for s in station_list:
        json_list.append("json/" + s.four_char_id + ".json")
    template = loader.get_template('nginfo/nettable.html')
    context = { 
        'station_list': station_list,
        'json_list'   : json_list
    }
    return HttpResponse(template.render(context, request))

def data_availability(request):
    station_list = GpsStation.objects.order_by('four_char_id')
    json_list = []
    for s in station_list: json_list.append(s.four_char_id+".json")
    #json_list = simplejson.dumps(json_list)
    template = loader.get_template('nginfo/data_availability.html')
    context = {
        'station_list': station_list,
        'json_list'   : json_list
    }
    return HttpResponse(template.render(context, request))

#def detail(request, gpsstation_id):
#    try:
#        station = GpsStation.objects.get(pk=gpsstation_id)
#    except:
#        raise Http404("Invalid station id!")
#    return HttpResponse("You 're looking at station %s" % gpsstation_id)
#
#def detail2(request, gpsstation_four_char_id):
#    user_in = "{:}".format(gpsstation_four_char_id)
#    try:
#        station = GpsStation.objects.get(four_char_id=gpsstation_four_char_id)
#    except:
#        raise Http404("Invalid station four_char_id: "+user_in)
#    return HttpResponse("You 're looking at station %s" % gpsstation_four_char_id)
#def stainfo(request, gpsstation_four_char_id):
#    station  = get_object_or_404(GpsStation, four_char_id=gpsstation_four_char_id)
#    position = get_object_or_404(Position, pk=station.xyz_position_id)
#    return render(request, 'nginfo/detail2.html', {'station': station, 'position': position})
#    return HttpResponse("You 're voting on question %s" % gpsstation_id)
