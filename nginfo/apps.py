from django.apps import AppConfig


class NginfoConfig(AppConfig):
    name = 'nginfo'
