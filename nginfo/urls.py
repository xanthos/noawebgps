from django.urls import path

from . import views

urlpatterns = [
    # ex: /nginfo/
    path('', views.index, name='index'),
    # ex: /nginfo/netmap/
    path('netmap/', views.netmap, name='netmap'),
    # ex: /nginfo/data_availability/
    path('data_availability/', views.data_availability, name='data_availability'),
    # ex: /nginfo/noamaps/
    path('noamaps/', views.noamaps, name='noamaps'),
    # ex: /nginfo/nettable/
    path('nettable/', views.nettable, name='nettable'),
    # ex: /nginfo/data/
    path('data/', views.data, name='data'),
    # ex: /nginfo/intro/
    path('intro/', views.intro, name='intro'),
    # ex: /nginfo/research/
    path('research/', views.research, name='research'),
    # ex: /nginfo/noa1/
    path('<str:gpsstation_four_char_id>/', views.station, name='station'),
    # ex: /nginfo/nettable/installation_date
    path('nettable/<slug:sort_by>/', views.nettable, name='nettable')
]
