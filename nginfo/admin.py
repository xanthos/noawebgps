from django.contrib import admin
from .models import Agency, Network, Position, GpsStation, StationPhoto, TimeSeries

admin.site.register(Agency)
admin.site.register(Network)
admin.site.register(Position)
admin.site.register(GpsStation)
admin.site.register(StationPhoto)
admin.site.register(TimeSeries)
