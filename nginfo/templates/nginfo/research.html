{% extends 'base.html' %}
{% block title %}Home{% endblock title %}
{% load staticfiles %} <!-- Prepare django to load static files -->
{% block content %}

<h2>RESEARCH</h2>

<p>The goal of measuring millimeter-scale displacements from the GNSS data 
requires rigorous analysis and parameter estimation techniques. At NOA we 
carry out the processing of the GNSS data using 
<a href="http://geoweb.mit.edu/gg/">GAMIT/GLOBK</a> and 
<a href="http://geoweb.mit.edu/gg/">TRACK</a> which form a comprehensive suite 
of programs for analyzing GNSS measurements. We use this software package to 
obtain daily network solutions from conventional low-rate GPS data, as well 
as to perform epoch by epoch kinematic analysis of high‐rate GPS data. Some 
fundamental examples of the utilization of the GNSS data in geosciences 
consist of deriving three dimensional velocity fields that can be used for 
further geodynamics and geo-kinematics applications and modeling, and 
retrieving the coseismic static and dynamic displacements that accompany the 
occurrence of moderate and strong earthquakes which can be used in earthquake 
seismology and seismogeodesy applications. At NOA we use the NOANET data to 
conduct research into the fields of tectonic geodesy and GNSS seismology. 
Below we present some information about the procedures we use to analyze GNSS 
data, although more details on the processing approaches for both kinds of 
data can be found in the references provided at the end of this section.</p>

<div class="text-center">
  <img src="{% static "img/research-1.jpg" %}" class="img-thumbnail" style="width: 50%;">
  <div class="caption">
    <p><i>Horizontal GPS velocity vectors relative to stable Eurasia. 
          Error ellipses represent 95% confidence level 
         (from <a href="#ref2015">Chousianitis et al. 2015</a>).</i></p>
  </div>
</div>

<p>We process the low-rate GNSS observations in a three-step approach. In the 
first step, we analyze doubly differenced GPS phase observations with GAMIT in 
24h sessions to estimate loosely constrained station coordinates together with 
orbital and Earth orientation parameters and their associated covariance 
matrices. We include data from several IGS stations in our analysis so as to 
tie our regional network to the global GPS network. For these sites, we set 
tight constraints on their a priori values, while the coordinates for the rest 
stations are allowed to vary freely by way of very loose constraints. For each 
session, we obtain two solutions based on phase ambiguity resolution, one 
bias-free and one bias-fixed, along with the associated variance-covariance 
matrices. In the second step, we use the daily loosely constrained bias-fixed 
solutions of the parameters and their associated covariance matrices as 
quasi-observations in GLOBK, which is a smoothing Kalman filter, to generate 
position time series. The reference frame is defined using the IGS stations 
incorporated in the GAMIT processing part by minimizing on each day in a least 
squares sense the difference between their estimated positions and the 
positions implied by their a priori coordinates (position and velocity). We 
use a number of iterations to eliminate bad sites and to compute station 
weights for the reference frame stabilization. The derived position time 
series are subsequently modeled to estimate a constant velocity term (linear 
trend) together with annual and semiannual sinusoidal variations for each 
component and possible offsets at specific epochs. The successful modeling of 
these first-order features observed in time series leads to accurate and 
realistic determination of geodetic velocities. Finally, we account for 
time-correlated errors in our processed time series in order to present 
realistic velocity uncertainties.</p>

<p>The high-rate GNSS data are processed with the kinematic positioning 
program TRACK. We use floating point LC estimates and the Melbourne-Wubbena 
wide lane to resolve ambiguities to integer values with the assistance of a 
Kalman smoothing filter. The processing is performed using IGS final orbits, 
IGS final ionospheric total electron content grid to yield ionospheric delay 
estimates, differential code biases, and absolute antenna phase center models. 
A fixed reference station is required to produce the kinematic solutions, 
which in the case of processing GNSS data to obtain coseismic static and 
dynamic displacements, should be far enough from the epicenter to guarantee no 
influence by the earthquake shaking. After the run a check in the residuals of 
each satellite is vital to remove those that exhibit large systematic values, 
and finally the LC RMS scatter at all processed stations should be checked and 
confirmed that it is low enough so as to imply a robust solution. We also 
process data from the previous days with respect to the earthquake occurrence 
day in order to filter the time series of the stations that captured the 
coseismic displacements. The reasoning for this is to reduce the systematic 
station-specific errors caused by daily repeatable noise such as multipath. For 
this purpose we apply the modified sidereal filtering to account for the time 
offset of the orbit repeat period compared to the sidereal period.</p>

<div class="text-center">
  <img src="{% static "img/research-2.jpg" %}" class="img-thumbnail" style="width: 70%;">
  <div class="caption">
    <p><i><b>(a)</b> Surface projection of the best fitting coseismic slip 
        distribution of the 12 June 2017 Mw = 6.3 earthquake estimated from 
        the joint inversion of conventional and high-rate GPS data. <b>(b)</b> 
        Fault plane view of the cumulative slip distribution. <b>(c)</b> 
        Waveform modeling of the high-rate GPS records. 
        (<a href="#ref2018">Chousianitis and Konca 2018</a>).</i></p>
  </div>
</div>

<h4>References</h4>

<p><a name="ref2013">[1]</a> Chousianitis, K., A. Ganas, and M. Gianniou (2013), <i>Kinematic interpretation of present‐day crustal deformation in central Greece from continuous GPS measurements</i>, Journal of Geodynamics, 71, 1–13.</p>

<p><a name="ref2015">[2]</a> Chousianitis, K., A. Ganas, and C. P. Evangelidis (2015), <i>Strain and rotation rate patterns of mainland Greece from continuous GPS data and comparison between seismic and geodetic moment release</i>, Journal of Geophysical Research: Solid Earth, 120, 3909–3931, doi:10.1002/2014JB011762.</p>

<p><a name="ref2016">[3]</a> Chousianitis, K., A. O. Konca, G.‐A. Tselentis, G. A. Papadopoulos, and M. Gianniou (2016), <i>Slip model of the 17 November 2015 Mw = 6.5 Lefkada earthquake from the joint inversion of geodetic and seismic data</i>, Geophysical Research Letters, 43, 7973–7981, doi: 10.1002/2016GL069764.</p>

<p><a name="ref2018">[4]</a> Chousianitis, K., & Konca, A. O. (2018). <i>Coseismic slip distribution of the 12 June 2017 Mw = 6.3 Lesvos earthquake and imparted static stress changes to the neighboring crust</i>, Journal of Geophysical Research: Solid Earth, 123, 8926–8936, https://doi.org/10.1029/2018JB015950</p>

{% endblock content %}
