#! /usr/bin/python
# coding: utf8

# from __future__ import print_function
import json
import datetime
import sys, os

if not sys.argv[1] or not os.path.isfile(sys.argv[1]):
    print("[ERROR] Inocrrect/missing input json file")

with open(sys.argv[1]) as json_in: data = json.load(json_in)
data_sorted = sorted(data, key=lambda x: datetime.datetime.strptime(
    x["epoch"], "%Y-%m-%d %H:%M:%S"))
json.dump(data_sorted, sys.stdout)
